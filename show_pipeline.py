# a first small script to see the pipeline state db 
# 12/04/19 patgendre

import emission.core.get_database as edb
import emission.core.wrapper.entry as ecwe
all_users = edb.get_uuid_db().find({}, {"user_email":1, "uuid": 1, "_id": 0})



from datetime import datetime as dt
stage=[0,6,1,2,3,11,4,9]
stage_name=['USERCACHE','FILTERING','TRIP SEGM.','SECTION SEGM.','SMOOTHG','CLEAN','MODE INF.','OUTPUT']

for u in all_users:
  nom=u['user_email']
  id=u['uuid']
  print('****** utilisateur ',nom)
  states=list(edb.get_pipeline_state_db().find({'user_id':id}))
  ds={}
  for s in states:
    ds[s['pipeline_stage']]=[s['curr_run_ts'],s['last_processed_ts'],s['last_ts_run']]
  last=0.0
  for i in range(8):
    s=stage[i]
    sname=stage_name[i]
    if s not in ds:
      print ('absence de ',sname)
    else:
      if not (ds[s][0] is None): print('TRAITEMENT EN COURS:', ds[s][0])
      if (ds[s][1] is None):
        print("AUCUNE DATE de dernière donnée traitée à l'étape", sname)
      else:
        print('étape ',sname, ': traitée à:', dt.fromtimestamp(ds[s][2]), 'dernière donnée traitée le',dt.fromtimestamp(ds[s][1]).date())
        if last>0 and last>ds[s][2]:
          print ('ATTENTION: le dernier ts de traitement devrait être > au TS précédent')
      if ds[s][2] is None:
          last=0
      else:
          last=ds[s][2]
