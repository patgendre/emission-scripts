# a first small script to see on which data there is data for all users
# 23/04/19 patgendre


import emission.core.get_database as edb
import emission.core.wrapper.entry as ecwe
#all_users = edb.get_uuid_db().find({}, {"user_email":1, "uuid": 1, "_id": 0})

from datetime import datetime as dt

import emission.core.get_database as edb
import emission.storage.timeseries.abstract_timeseries as esta
import emission.storage.decorations.analysis_timeseries_queries as esda
import emission.core.wrapper.entry as ecwe
import emission.storage.decorations.trip_queries as esdt
import emission.storage.timeseries.timequery as estt
import pandas as pd
all_users = pd.DataFrame(list(edb.get_uuid_db().find({}, {"user_email":1, "uuid": 1, "_id": 0})))
ts = esta.TimeSeries.get_time_series(all_users.iloc[0].uuid)

#backgdloc_df = ts.get_data_df("background/location", time_query=None)
# pandas intéressant pour des stats mais pas forcément pour des infos basiques sur les données??
# https://pandas.pydata.org/pandas-docs/stable/getting_started/10min.html


for u in range(all_users.count().uuid):
  print('****** utilisateur ',all_users.iloc[u].user_email)
  print(all_users.iloc[u].uuid)
  #values=list(edb.get_analysis_timeseries_db().find({'user_id':id}))
  #keys=edb.get_analysis_timeseries_db().distinct("metadata.key")
  #print(keys)
  keys=edb.get_analysis_timeseries_db().find({"user_id":all_users.iloc[u].uuid}).distinct("metadata.key")  
  ts = esta.TimeSeries.get_time_series(all_users.iloc[u].uuid)
  if "analysis/cleaned_trip" not in keys:
        print("Pas de déplacements analysés! Vérifier les données?")
        continue
  trips=ts.get_data_df("analysis/cleaned_trip")
  j=trips.start_local_dt_day.astype(str)+"/"+trips.start_local_dt_month.astype(str)+"/"+trips.start_local_dt_year.astype(str)
  j=j.drop_duplicates().tolist()
  print(trips.count()._id.astype(str)+" déplacements aux dates suivantes: ",j)
  if "analysis/cleaned_section" not in keys:
    print("Pas de trajets!")
  else:
    sections=ts.get_data_df("analysis/cleaned_section")
    print(sections.count()._id.astype(str)," trajets.")
  if "analysis/inferred_section" not in keys:
    print("Pas de trajets analysés par la détection de mode! Vérifier le pipeline?")
  else:
        sections=ts.get_data_df("analysis/inferred_section")
        print(sections.count()._id.astype(str)," trajets analysés.")
  if "analysis/cleaned_place" not in keys:
        print("Pas d'OD!?")
  else:
        places=ts.get_data_df("analysis/cleaned_place")
        print(places.count()._id.astype(str)," origines/destinations.")
  if "analysis/cleaned_stop" not in keys:
        print("Pas d'arrêts?")
  else:
        stops=ts.get_data_df("analysis/cleaned_stop")
        print(stops.count()._id.astype(str)," arrêts intermédiaires.")
